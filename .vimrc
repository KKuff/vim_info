" disable any features not supported with current version
set nocompatible

" show line numbers
set number

" enable auto-indent
set ai

" enable smart-indent
set si

" set tabs to 2 spaces wide
set tabstop=2
set shiftwidth=2

" change tabs to spaces
set expandtab

" enable autocomplete for vim commands
set wildmenu
set wildmode=longest:full,full

" detect filetype
filetype on
filetype plugin on

" enable autocomplete
set omnifunc=syntaxcomplete#Complete
set completeopt=longest,menuone
set grepprg=grep\ -nH\ $*

" show command details
set showcmd

" folding
set foldmethod=marker

" turn off highlighting for searched terms
set nohlsearch

" highlight as we search
set incsearch

" ignore case for searches
set ignorecase

" don't ignore case if there is a capital letter in search term
set smartcase

"show matching brackets
set showmatch

set background=dark
colorscheme wombat256
